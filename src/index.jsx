import React from 'react';
import { render } from 'react-dom';
import { HashRouter, Routes, Route } from 'react-router-dom';
import { ThemeProvider as StyledProvider } from 'styled-components';

import Container from '@material-ui/core/Container';
import deepOrange from '@material-ui/core/colors/deepOrange';
import lightBlue from '@material-ui/core/colors/lightBlue';
import { ThemeProvider, createTheme, withStyles } from '@material-ui/core/styles';

import BuildOutlinedIcon from '@material-ui/icons/BuildOutlined';

import Nav from './components/Nav';
import routes from './pages';


const $theme = createTheme({
  palette: {
    type: 'dark',
    primary: {
      light: lightBlue[200],
      main: lightBlue[500],
      dark: lightBlue[800],
      contrastText: '#fff'
    },
    secondary: {
      light: deepOrange[300],
      main: deepOrange[500],
      dark: deepOrange[700],
      contrastText: '#fff'
    }
  }
});

const GlobalStyles = withStyles((theme) => ({
  '@global': {
    body: {
      background: theme.palette.background.default,
      color: theme.palette.getContrastText(theme.palette.background.default),
      margin: 0,
      overflow: 'hidden !important'
    },
    '#app': {
      display: 'flex',
      flexDirection: 'column',
      width: '100vw',
      height: '100vh',

      '& > .content': {
        height: '100%',
        overflow: 'hidden auto !important'
      }
    }
  }
}))(() => null);

render((
  <ThemeProvider theme={$theme}>
    <StyledProvider theme={$theme}>
      <GlobalStyles />

      <HashRouter basename="/">
        <Container id="app" disableGutters maxWidth={false}>
          <Nav routes={routes} />

          <Container className="content" disableGutters maxWidth={false}>
            <Routes>
              {routes.map(({ index = false, path, element }) => (
                <Route key={path} {...{ index, path, element }} />
              ))}
            </Routes>
          </Container>
        </Container>
      </HashRouter>
    </StyledProvider>
  </ThemeProvider>
), document.getElementById('app'));
