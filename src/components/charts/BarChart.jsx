import React from 'react';
import PropTypes from 'prop-types';
import AppcraftChart, { withSeriesData } from '@appcraft/graph/Chart';

import IMDB_DATA from '../../assets/data/imdb.json';


//* Highchart Options
const CHART_OPTIONS = {
  chart: {
      type: 'bar'
  },
  title: {
    text: 'Area Chart by Director'
  },
  subtitle: {
    text: 'Area Example'
  },
  yAxis: {
    title: {
      text: 'Count'
    }
  }
};


//* Components
const SeriesChart = withSeriesData({
  aggregate: 'sum',
  series: '{{ (director && `${director.charAt(0).toUpperCase()}${director.slice(1)}`) || "Unknow" }}',
  stack: 'director',
  x: 'actor',
  y: ['movies', 'tvs']
})(AppcraftChart);

const BarChart = ({ theme }) => (
  <SeriesChart theme={theme} options={CHART_OPTIONS} data={IMDB_DATA} />
);

BarChart.displayName = 'BarChart';

BarChart.propTypes = {
  theme: PropTypes.oneOfType([
    'avocado',
    'dark-blue',
    'dark-green',
    'dark-unica',
    'gray',
    'grid',
    'sunset'
  ]).isRequired
};

export default BarChart;
