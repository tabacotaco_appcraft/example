import React from 'react';
import PropTypes from 'prop-types';
import AppcraftChart, { withSeriesData } from '@appcraft/graph/Chart';

import IMDB_DATA from '../../assets/data/imdb.json';


//* Highchart Options
const CHART_OPTIONS = {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Column Chart by Director/Actor'
  },
  subtitle: {
    text: 'Column Example'
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle'
  },
  yAxis: {
    title: {
      text: 'Count'
    }
  },
  plotOptions: {
    column: {
      stacking: 'normal'
    }
  }
};


//* Components
const SeriesChart = withSeriesData({
  aggregate: 'sum',
  series: '{{ (director && `${director.charAt(0).toUpperCase()}${director.slice(1)}`) || "Unknow" }} & {{ (actor && `${actor.charAt(0).toUpperCase()}${actor.slice(1)}`) || "Unknow" }}({{ series.stack }})',
  stack: '{{ series.y.field }}',
  x: 'year',
  y: ['movies', 'tvs']
})(AppcraftChart);

const ColumnChart = ({ theme }) => (
  <SeriesChart theme={theme} options={CHART_OPTIONS} data={IMDB_DATA} />
);

ColumnChart.displayName = 'ColumnChart';

ColumnChart.propTypes = {
  theme: PropTypes.oneOfType([
    'avocado',
    'dark-blue',
    'dark-green',
    'dark-unica',
    'gray',
    'grid',
    'sunset'
  ]).isRequired
};

export default ColumnChart;
