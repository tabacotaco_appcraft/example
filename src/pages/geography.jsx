import React, { useRef } from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import Geography from '@appcraft/graph/Geography';


//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  container: {
    height: '100%',

    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  },
  map: {
    height: '100%'
  }
}));


//* Component
export default function GeographyPage() {
  const geographyRef = useRef();
  const classes = useStyles();

  return (
    <Container maxWidth={false} className={classes.container}>
      <Geography
        ref={geographyRef}
        lang="en"
        className={classes.map}
        images={{
          airplane: `${__WEBPACK_DEFINE__.BASENAME}/resouces/images/ic_airplane.png`,
          car: `${__WEBPACK_DEFINE__.BASENAME}/resouces/images/ic_car.png`,
          person: `${__WEBPACK_DEFINE__.BASENAME}/resouces/images/ic_person.png`
        }}
        options={{
          zoom: 12,
          center: [121.547722, 25.047407]
        }}
        DataMarkerControllerProps={{
          data: [{
            id: 'kmSFjf',
            group: 'case',
            icon: 'car',
            label: 'AXC-3440',
            coordinates: [{ lat: 25.0262742, lng: 121.5046093 }]
          }, {
            id: 'kmSFjh',
            group: 'case',
            icon: 'person',
            label: 'A123456789\n壞人',
            coordinates: [{ lat: 25.0318484, lng: 121.5011661 }]
          }, {
            id: 'kmSFkI',
            group: 'track',
            icon: 'airplane',
            label: 'CI602 - 中華航空',
            coordinates: [{ lat: 24.988512, lng: 121.560068 }]
          }]
        }}
        HeatMapPickerProps={{
          value: 'point',
          options: {
            area: {
              source: 'point'
            },
            point: {
              paint: {
                'heatmap-weight': ['interpolate', ['linear'], ['get', 'mag'], 0, 0, 6, 1],
                'heatmap-radius': ['interpolate', ['linear'], ['zoom'], 0, 2, 9, 20]
              }
            }
          }
        }}
        PolygonSelectionProps={{
          value: ['new-taipei'],
          options: [{
            id: 'taipei',
            description: 'Taipei',
            geojson: `${__WEBPACK_DEFINE__.BASENAME}/resouces/json/taipei-geojson.json`,
            label: {
              layout: { 'text-field': ['get', 'TNAME'] }
            }
          }, {
            id: 'new-taipei',
            description: 'New Taipei',
            geojson: `${__WEBPACK_DEFINE__.BASENAME}/resouces/json/new-taipei-geojson.json`,
            label: {
              layout: { 'text-field': ['get', 'TOWNNAME'] }
            }
          }]
        }}
        StylePickerProps={{
          options: [{
            id: 'streets',
            description: 'Streets',
            style: 'https://api.maptiler.com/maps/streets/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL'
          }, {
            id: 'satellite',
            description: 'Satellite',
            style: 'https://api.maptiler.com/maps/hybrid/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL'
          }, {
            id: 'grayscale',
            description: 'Grayscale',
            style: 'https://api.maptiler.com/maps/positron/style.json?key=get_your_own_OpIi9ZULNHzrESv6T2vL'
          }]
        }}
      />
    </Container>
  );
}
