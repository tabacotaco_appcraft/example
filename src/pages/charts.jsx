import React, { useState } from 'react';

import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';

import BarChart from '../components/charts/BarChart';
import ColumnChart from '../components/charts/ColumnChart';


const THEMES = [
  'avocado',
  'dark-blue',
  'dark-green',
  'dark-unica',
  'gray',
  'grid',
  'sunset'
];

//* Custom Hooks
const useStyles = makeStyles((theme) => ({
  container: {
    overflow: 'hidden auto',
    height: '100%',

    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  }
}));

//* Component
export default function ChartsPage() {
  const [theme, setTheme] = useState(THEMES[3]);
  const classes = useStyles();

  return (
    <>
      <AppBar position="sticky" color="default">
        <ButtonGroup fullWidth variant="outlined" size="large" color="default">
          {THEMES.map((color) => (
            <Button key={color} {...(color === theme && { variant: 'contained', color: 'primary' })} onClick={() => setTheme(color)}>
              {color}
            </Button>
          ))}
        </ButtonGroup>
      </AppBar>

      <Container maxWidth={false} className={classes.container}>
        <BarChart theme={theme} />
        <ColumnChart theme={theme} />
      </Container>
    </>
  );
}
