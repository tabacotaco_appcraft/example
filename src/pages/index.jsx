import React from 'react';

import BarChartIcon from '@material-ui/icons/BarChart';
import BuildOutlinedIcon from '@material-ui/icons/BuildOutlined';
import PublicIcon from '@material-ui/icons/Public';

import ChartsPage from './charts';
import Designer from './designer';
import GeographyPage from './geography';

export default [
  { title: 'Designer', icon: (<BuildOutlinedIcon />), path: '/designer', element: (<Designer />) },
  { title: 'Charts', icon: (<BarChartIcon />), path: '/charts', element: (<ChartsPage />) },
  { title: 'Geography', icon: (<PublicIcon />), path: '/geography', element: (<GeographyPage />) }
];
